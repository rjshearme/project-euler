from string import ascii_lowercase

alphabet = list(ascii_lowercase[0:26])
names = open("sorted.txt",'r').read()
names_list = names.split("\n")[:-1]

def letter_values(name:str) -> int:
    name = list(name.lower())
    value = 0
    for letter in name:
        value += alphabet.index(letter) + 1
    return value


def index_value(name):
    return names_list.index(name) + 1


total = 0
for name in names_list:
    total += letter_values(name)*index_value(name)

print(total)
