def difference(limit):
    def sumSquare(limit):
        accum = 0
        for i in range(1,limit+1,1):
            accum += i*i
        return accum

    def sum(limit):
        accum = 0
        for i in range(1,limit+1,1):
            accum += i
        return accum*accum

    print(abs(sumSquare(limit) - sum(limit)))

difference(100)
