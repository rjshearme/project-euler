def testPal():
    print(isPal(1))
    print(isPal(11))
    print(isPal(121))
    print(isPal(125621))
    print(isPal(1231))

def isPal(number):
    string = str(number)
    if len(string) == 0 or len(string) == 1:
        return True
    elif string[0] != string[-1]:
        return False
    else:
        return(isPal(string[1:-1]))

answer = []
for i in range(999,1,-1):
    for j in range(999,1,-1):
        if isPal(i*j) == True:
            answer.append(i*j)
lPal = 0
for i in answer:
    if i > lPal:
        lPal = i

print(lPal)
