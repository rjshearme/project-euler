fibVals = [0,1]
def fibonacci():
    answer = 0
    while fibVals[-1] +fibVals[-2] <4000000:
        new = fibVals[-1] + fibVals[-2]
        fibVals.append(new)
    for val in fibVals:
        if val%2 == 0:
            answer += val
    print(answer)

fibonacci()
