factors = [1]

def factProd(factors):
    product = 1
    for factor in factors:
        product *= factor
    return product

def missingFact(number, factors):
    for factor in factors:
        if number%factor == 0:
            number = number // factor
    return number

for i in range(2,21,1):
    if factProd(factors) % i != 0:
        factors.append(missingFact(i, factors))

print(factors)
print(factProd(factors))
