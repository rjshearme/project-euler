import math
def Primes(maxPrime:int) -> list:
    """ Generates a list of the Primes up to the argument given """
    def genSetNum(num: int, maxPrime: int) -> set:
        return set(range(num*2,maxPrime+1,num))

    allSet = genSetNum(1,maxPrime) - genSetNum(2,maxPrime)
    for i in range(3,math.ceil(math.sqrt(maxPrime)),2):
        if i in allSet:
            tempSet = genSetNum(i, maxPrime)
            allSet = allSet - tempSet
    return(sorted(list(allSet)))

print(Primes(1000))
