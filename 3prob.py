import math
num = 600851475143
root = round(math.sqrt(num))+1
answers = []
final = []

for i in range(1, root):
    if num%i == 0:
        num = num//i
        answers.append(i)
