from math import ceil, sqrt


def divFinder(number:int) -> int:
    divisors = set()
    for i in range(1, ceil(sqrt(number))):
        if number % i == 0:
            divisors.add(i)
            divisors.add(number // i)
    return len(divisors)


def triNum(number:int) -> int:
    return (number*(number + 1))//2


i = 2
while divFinder(triNum(i)) < 500:
    i += 1
print(i, ":", triNum(i), ":", divFinder(triNum(i)))
