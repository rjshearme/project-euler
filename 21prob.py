from math import ceil, sqrt


def findDiv(number: int) -> set:
    divisors = {1}
    for i in range(2,ceil(sqrt(number)),1):
        if number%i == 0:
            divisors.add(i)
            divisors.add(number//i)
    return sum(divisors)


def amicability(number: int) -> bool:
    if number == findDiv(findDiv(number)) and number != findDiv(number):
        return True


amicables = set()
for i in range(2,10001):
    if amicability(i) == True:
        amicables.add(i)

print(sum(amicables))
